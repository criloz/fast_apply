#!/usr/bin/python
'''
Created on 25/04/2012

@author: cristian
'''

from gevent.pywsgi import WSGIServer
import json,cgi

def application(env, start_response):
    if env['PATH_INFO'] == '/':
        start_response('200 OK', [('Content-Type', 'text/html')])
        return ["""<div style="width:150px;margin:auto;">
                    <form method="POST">
                        <input type="text" name="name" value=""/><br>
                        <input type="submit" name="b_r" value="r" onclick="this.form.action='r'" />
                        <input type="submit" name="b_scipy" value="scipy" onclick="this.form.action='scipy'" />
                        </form>
                    </div>"""]
    if env['PATH_INFO'] == '/r':
        start_response('200 OK', [('Content-Type', 'text/html')])
        post = cgi.FieldStorage( fp=env['wsgi.input'],environ=env,keep_blank_values=True)
        name = post.getvalue("name", "")
        return [json.dumps({"message": "Posted name=%s to /r"%name})]
    if env['PATH_INFO'] == '/scipy':
        start_response('200 OK', [('Content-Type', 'text/html')])
        post = cgi.FieldStorage( fp=env['wsgi.input'],environ=env,keep_blank_values=True)
        name = post.getvalue("name", "")
        return [json.dumps({"message": "Posted name=%s to /scipy"%name})]
        return ["<b>hello world</b>"]
    else:
        start_response('404 Not Found', [('Content-Type', 'text/html')])
        return ['<h1>Not Found</h1>']


if __name__ == '__main__':
    print 'Serving on 8080...'
    WSGIServer(('', 8080), application).serve_forever()